require("babel-polyfill");
const BigNumber = require('@0xproject/utils').BigNumber;
const ContractWrappers = require('0x.js').ContractWrappers;
const ZeroEx = require('0x.js');
const PrivateKeyWalletSubprovider = require('@0xproject/subproviders').PrivateKeyWalletSubprovider;
const Web3ProviderEngine = require('web3-provider-engine');
const RpcSubprovider = require('web3-provider-engine/subproviders/rpc');
const Web3Wrapper = require('@0xproject/web3-wrapper').Web3Wrapper;
const axios = require('axios');

const EXCHANGE_ADDRESS = "0x4f833a24e1f95d70f028921e27040ca56e09ab0b";
const NULL_ADDRESS = "0x0000000000000000000000000000000000000000";
const STARBIT_TAKER_ADDRESS = '0x0681e844593a051e2882ec897ecd5444efe19ff2';
const STARBIT_FEE_ADDRESS = '0x0681e844593a051e2882ec897ecd5444efe19ff2';
// const SERVER_URL = "http://localhost:5000";
const SERVER_URL = "https://www.starbitex.com/";
let exchangeRate = {};

async function _getExchangeRateAsync(apiKey) {
    let rate = await axios({
        method: "get",
        baseURL: SERVER_URL,
        url: '/api/tokenExchange/',
        headers: {
            'User-Agent': `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99`,
            'Content-Type': 'application/json',
            'API-Key': apiKey
        }
    });
    rate.data.data.forEach(v => {
        exchangeRate[v.address] = v;
    });

    return exchangeRate;
}

async function _getGASAsync(apiKey) {
    let zrx = await axios({
        method: "get",
        baseURL: SERVER_URL,
        url: '/api/gasPrice/',
        headers: {
            'User-Agent': `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99`,
            'Content-Type': 'application/json',
            'API-Key': apiKey
        }
    });
    return zrx.data.data.gasPrice;
}

async function _checkKey(apiKey) {
    let result = await axios({
        method: "post",
        baseURL: SERVER_URL,
        url: '/api/company/check',
        data: {
            apikey: apiKey
        },
        headers: {
            'User-Agent': `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99`,
            'Content-Type': 'application/json',
            'API-Key': apiKey
        }
    });
    return result.data.result;
}

function StarBitSDK(args) {
    if (args.privateKey === undefined || args.rpcUrl === undefined)
        throw `The arguments should have "priateKey" and "rpcUrl" `;
    if (args.apiKey === undefined)
        throw `The arguments should have "apiKey" `;
    this.apiKey = args.apiKey;
    this.provider = new Web3ProviderEngine();
    this.praviteKey = new PrivateKeyWalletSubprovider(args.privateKey)
    this.provider.addProvider(this.praviteKey)
    this.provider.addProvider(new RpcSubprovider({ rpcUrl: args.rpcUrl }));
    this.provider.start()
    this.zeroEx = new ContractWrappers(this.provider, { networkId: 1 });
    this.web3Wrapper = new Web3Wrapper(this.provider)
    this.defaultAccount = "";
}

StarBitSDK.prototype.getAllTokenAsync = async function (baseToken) {
    let url = baseToken ? `/api/token/${baseToken}` : `/api/token`;
    let orders = await axios({
        method: "get",
        baseURL: SERVER_URL,
        url: url,
        headers: {
            'User-Agent': `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99`,
            'Content-Type': 'application/json',
            'API-Key': this.apiKey
        }
    });
    return orders.data;
}

StarBitSDK.prototype.getOrderBookAsync = async function (baseToken, tradingToken) {
    let orders = await axios({
        method: "get",
        baseURL: SERVER_URL,
        url: `/api/v2/order/${baseToken}/${tradingToken}`,
        headers: {
            'User-Agent': `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99`,
            'Content-Type': 'application/json',
            'API-Key': this.apiKey
        }
    });
    return orders.data;
}

StarBitSDK.prototype.getWalletOrdersAsync = async function () {
    let wallet = await axios({
        method: "get",
        baseURL: SERVER_URL,
        url: `/api/v2/wallet/${this.defaultAccount}`,
        headers: {
            'User-Agent': `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99`,
            'Content-Type': 'application/json',
            'API-Key': this.apiKey
        }
    })
    return wallet.data;
}

StarBitSDK.prototype.refreshDefaultAccountAsync = async function () {
    let addresses = await this.web3Wrapper.getAvailableAddressesAsync();
    this.defaultAccount = addresses[0].toLowerCase();
    return this.defaultAccount;
}

StarBitSDK.prototype.createOrderAsync = async function (baseTokenAddress, baseTokenAmount, baseTokenDecimal, tradingTokenAddress, tradingTokenAmount, tradingTokenDecimal, isBuyOrder, expirationInMin) {
    this.defaultAccount = await this.refreshDefaultAccountAsync();
    const baseTokenAmountInDecimal = Web3Wrapper.toBaseUnitAmount(new BigNumber(baseTokenAmount), +baseTokenDecimal);
    const tradingtokenAmountInDecimal = Web3Wrapper.toBaseUnitAmount(new BigNumber(tradingTokenAmount), +tradingTokenDecimal);
    let [gas, exchangeRate] = await Promise.all([_getGASAsync(this.apiKey), _getExchangeRateAsync(this.apiKey)]);
    gas = (+gas + 1) * 1e9;
    let isSTABLE = !!exchangeRate[tradingTokenAddress];
    if (!exchangeRate[baseTokenAddress]) throw Error(`${baseTokenAddress} cant be a base token`);
    const order = {
        exchangeAddress: EXCHANGE_ADDRESS,
        expirationTimeSeconds: new BigNumber((Date.now() / 1000 + 60 * expirationInMin).toFixed(0)),
        feeRecipientAddress: STARBIT_FEE_ADDRESS,
        makerAddress: this.defaultAccount,
        makerFee: Web3Wrapper.toBaseUnitAmount(new BigNumber(0), 18),
        salt: ZeroEx.generatePseudoRandomSalt(),
        senderAddress: NULL_ADDRESS,
        takerAddress: STARBIT_TAKER_ADDRESS,
        takerFee: Web3Wrapper.toBaseUnitAmount(new BigNumber(0), 18),
    }
    let price;
    if (isSTABLE == 0 && isBuyOrder == 1) {
        //小幣對穩定幣(買單)
        order.fee = new BigNumber((100000 * gas / exchangeRate[baseTokenAddress].toEth).toFixed(0)).plus(new BigNumber(baseTokenAmountInDecimal).times(new BigNumber(0.0015)))
        order.makerAssetData = ZeroEx.assetDataUtils.encodeERC20AssetData(baseTokenAddress);
        order.takerAssetData = ZeroEx.assetDataUtils.encodeERC20AssetData(tradingTokenAddress);
        order.makerAssetToken = baseTokenAddress;
        order.takerAssetToken = tradingTokenAddress;
        order.makerAssetAmount = baseTokenAmountInDecimal.plus(order.fee);
        order.takerAssetAmount = tradingtokenAmountInDecimal;
        order.balance = baseTokenAmountInDecimal
        price = baseTokenAmountInDecimal.div(tradingtokenAmountInDecimal)
    } else if (isSTABLE == 0 && isBuyOrder == 0) {
        //小幣對穩定幣(賣單)
        order.fee = new BigNumber((100000 * gas / exchangeRate[baseTokenAddress].toEth).toFixed(0)).plus(new BigNumber(baseTokenAmountInDecimal).times(new BigNumber(0.0015)))
        order.makerAssetData = ZeroEx.assetDataUtils.encodeERC20AssetData(tradingTokenAddress);
        order.takerAssetData = ZeroEx.assetDataUtils.encodeERC20AssetData(baseTokenAddress);
        order.makerAssetToken = tradingTokenAddress;
        order.takerAssetToken = baseTokenAddress;
        order.makerAssetAmount = tradingtokenAmountInDecimal;
        order.takerAssetAmount = baseTokenAmountInDecimal.sub(order.fee);
        order.balance = baseTokenAmountInDecimal
        price = baseTokenAmountInDecimal.div(tradingtokenAmountInDecimal)
    } else if (isSTABLE == 1 && isBuyOrder == 1) {
        //穩定幣對穩定幣(買單)))
        order.fee = new BigNumber((100000 * gas / exchangeRate[baseTokenAddress].toEth).toFixed(0)).plus(new BigNumber(baseTokenAmountInDecimal).times(new BigNumber(0.0015)))
        order.makerAssetData = ZeroEx.assetDataUtils.encodeERC20AssetData(baseTokenAddress);
        order.takerAssetData = ZeroEx.assetDataUtils.encodeERC20AssetData(tradingTokenAddress);
        order.makerAssetToken = baseTokenAddress;
        order.takerAssetToken = tradingTokenAddress;
        order.makerAssetAmount = baseTokenAmountInDecimal.plus(order.fee);
        order.takerAssetAmount = tradingtokenAmountInDecimal;
        order.balance = baseTokenAmountInDecimal
        price = baseTokenAmountInDecimal.div(tradingtokenAmountInDecimal)
    } else if (isSTABLE == 1 && isBuyOrder == 0) {
        //穩定幣對穩定幣(賣單)
        order.fee = new BigNumber((100000 * gas / exchangeRate[tradingTokenAddress].toEth).toFixed(0)).plus(new BigNumber(tradingtokenAmountInDecimal).times(new BigNumber(0.0015)))
        order.makerAssetData = ZeroEx.assetDataUtils.encodeERC20AssetData(tradingTokenAddress);
        order.takerAssetData = ZeroEx.assetDataUtils.encodeERC20AssetData(baseTokenAddress);
        order.makerAssetToken = tradingTokenAddress;
        order.takerAssetToken = baseTokenAddress;
        order.makerAssetAmount = tradingtokenAmountInDecimal.plus(order.fee);
        order.takerAssetAmount = baseTokenAmountInDecimal;
        order.balance = tradingtokenAmountInDecimal
        price = baseTokenAmountInDecimal.div(tradingtokenAmountInDecimal)
    }

    const orderHash = ZeroEx.orderHashUtils.getOrderHashHex(order);
    const signature = await ZeroEx.signatureUtils.ecSignHashAsync(this.provider, orderHash, this.defaultAccount);


    const signedOrder = {
        ...order,
        signature,
        orderHash,
        price,
        gas
    };
    // console.log(signedOrder)
    return signedOrder;
}

StarBitSDK.prototype.sendOrderAsync = async function (order) {
    if (!this.defaultAccount) throw Error(`wallet not found`);

    let body = JSON.stringify({
        signature: order.signature,
        expirationTimeSeconds: order.expirationTimeSeconds.toNumber(),
        makerAddress: order.makerAddress,
        makerAssetAmount: order.makerAssetAmount.toString(),
        makerAssetData: order.makerAssetData,
        makerAssetToken: order.makerAssetToken,
        salt: order.salt.toString(),
        takerAssetData: order.takerAssetData,
        takerAssetAmount: order.takerAssetAmount.toString(),
        takerAssetToken: order.takerAssetToken,
        orderhash: order.orderHash,
        price: order.price.toString(),
        exchangeAddress: '0x4f833a24e1f95d70f028921e27040ca56e09ab0b',
        senderAddress: '0x0000000000000000000000000000000000000000',
        takerFee: order.takerFee.toString(),
        makerFee: order.makerFee.toString(),
        takerAddress: '0x0681e844593a051e2882ec897ecd5444efe19ff2',
        feeRecipientAddress: '0x8124071f810d533ff63de61d0c98db99eeb99d64',
        balance: order.balance,
        fee: order.fee,
        gas: order.gas,
        apiKey: this.apiKey
    });

    return axios({
        method: "post",
        baseURL: SERVER_URL,
        data: body,
        url: "/api/order/insert",
        headers: {
            'User-Agent': `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99`,
            'Content-Type': 'application/json',
            'API-Key': this.apiKey
        }
    }).then(res => {
        return res.data;
    });
}

StarBitSDK.prototype.cancelOrderAsync = async function (orderHash) {
    let signature = await this.web3Wrapper.signMessageAsync(this.defaultAccount, orderHash)
    let orderToCancel = {
        maker: this.defaultAccount,
        orderHash: orderHash,
        signedMessage: signature
    }
    return orderToCancel
}

StarBitSDK.prototype.sendCancelOrderAsync = async function (orderToCancel) {
    if (!this.defaultAccount) throw Error(`wallet not found`);

    let body = JSON.stringify({
        maker: orderToCancel.maker,
        orderHash: orderToCancel.orderHash,
        signedMessage: orderToCancel.signedMessage, //簽署資料後的簽章
    });

    return axios({
        method: "post",
        baseURL: SERVER_URL,
        data: body,
        url: "/api/order/cancel",
        headers: {
            'User-Agent': `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99`,
            'Content-Type': 'application/json',
            'API-Key': this.apiKey
        }
    }).then(res => {
        return res.data;
    });
}

async function createInstance(apiKey, privateKey, rpcUrl) {
    let wrapperWeb3 = new StarBitSDK({ apiKey, privateKey, rpcUrl });
    let [exchangeRate, defaultAccount, isKeyValid] = await Promise.all([
        _getExchangeRateAsync(apiKey),
        wrapperWeb3.refreshDefaultAccountAsync(),
        _checkKey(apiKey)
    ]);
    if (!isKeyValid)
        throw Error("API Key is not valid, please register with STAR BIT SERVICE(service@sdabi.com)")
    return wrapperWeb3;
}

module.exports = createInstance;