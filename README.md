# STAR BIT SDK

用於 [STAR BIT](https://www.starbitex.com/trade) 之程式交易SDK。  
使用前請先向STAR BIT申請API Key。  
將申請資料傳送郵件至 [STAR BIT SERVICE](mailto:service@sdabi.com)，在經過審核後，會回覆API Key給您。

# 安裝
> npm istall --save star-bit-sdk

# 測試
不含傳送資料到伺服器
> npm run test

# 範例
> **注意非同步問題**
## 初始化SDK環境
Promise Type:
```javascript
const Creator = require("star-bit-sdk");
let starInstance;
const sbtAddress = "0x503f9794d6a6bb0df8fbb19a2b3e2aeab35339ad",
  wethAddress = "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2";
Creator(
  <<API Key>>,
  <<Private Key>>,
  <<RPC URL>>
).then(starSDK => {
  //store a sdk instance in variable
  starInstance = starSDK;
  return starInstance.getOrderBookAsync(sbtAddress, wethAddress);
}).then(result =>{
  console.log(result);
})
```
async/await Type:
```javascript
const Creator = require("star-bit-sdk");
async function main() {
  const sbtAddress = "0x503f9794d6a6bb0df8fbb19a2b3e2aeab35339ad",
    wethAddress = "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2";

  const starInstance = await Creator(
    <<API Key>>,
    <<Private Key>>,
    <<RPC URL>>);

  let orders = await starInstance.getOrderBookAsync(sbtAddress, wethAddress);

  console.log(orders);
}
main();
```

## 功能說明
### 存取 STAR BIT API
#### 1. getAllTokenAsync  
取得 STAR BIT 上架的所有token

輸入：
  - baseToken: `String` `可選輸入，預設為WETH的代幣地址` 以該代幣為基準幣，查詢交易的成交量、成交價、漲跌幅等。

輸出： `Promise`
```json
  {
    "result": Boolean,
    "data": [ {
      //代幣的名稱
      "name": String,
      //代幣的縮寫
      "symbol": String,
      //代幣的精度
      "decimals": Number,
      //代幣的地址
      "tokenAddress": String,
      //代幣的狀態
      "status": String,
      //代幣對不同基準幣的資料
      "toStable":{
        //對WETH的資料
        "WETH": {
          //漲跌幅
          "raise": String,
          //成交量（若未成交則為"--"）
          "volume": String or Number,
          //成交價
          "price": String
        },
        //依照不同的基準幣，動態增加的資料
        << baseTokenName >>:{ __與WETH各式相同__ }
      }
    } ]
  }
```

#### 2. getOrderBookAsync  
取得相關代幣的買賣掛單

輸入： 

  - baseToken: `String` 作為基準的代幣地址
  - tradingToken: `String` 交易的代幣地址

輸出： `Promise`
```json
  {
    "result": Boolean,
    "data": {
      "buy": [ {
        //建立該單的時間戳
        "createTime": Number ,
        //該掛單過期的時間戳
        "expirationTimeSeconds": Number,
        //掛單的代幣數量
        "makerAssetAmount": String,
        //掛單的代幣地址
        "makerAssetToken": String,
        //吃單的代幣數量
        "takerAssetAmount": String,
        //吃單的代幣地址
        "takerAssetToken": String,
        //價格（掛單數量/吃單數量）
        "price": Number
      } ],
      "sell":[ { __與買單相同__ } ]
    }
  }
```

#### 3. getWalletOrdersAsync  
取得該錢包的所有掛單與歷史掛單 **（若沒有掛過任何單，則回傳404）**

輸出： `Promise`
```json
  {
    "result": Boolean,
    "data": {
      //錢包地址
      "address": String,
      //狀態尚未確認的總掛單數
      "orderMaxCount": Number,
      //狀態尚未確認的總掛單數，取前10筆
      "orders": [ { 
        //掛單的錢包地址
        "makerAddress": String,
        //掛單的狀態
        "status": String,
        //掛單的orderHash，用於取消掛單
        "orderHash": String,
        //創建掛單的時間戳
        "createTime": Number,
        //掛單過期的時間戳
        "expirationTimeSeconds": Number,
        //掛單的代幣數量
        "makerAssetAmount": String,
        //掛單的代幣地址
        "makerAssetToken": String,
        //掛單的代筆位數
        "makerAssertDecimal": Number,
        //掛單的原始代幣數量
        "makerAssertOriginalAmount": String,
        //吃單的代幣數量
        "takerAssetAmount": String,
        //吃蛋的代幣地址
        "takerAssetToken": String,
        //吃單的代筆位數
        "takerAssertDecimal": Number,
        //吃單的原始代幣數量
        "takerAssertOriginalAmount": String,
        //價格（掛單數量/吃單數量）
        "price": Number,
      } ],
      //掛單已經確認，不可更改狀態的總掛單數
      "doneOrderMaxCount": Number,
      //掛單已經確認，不可更改狀態的掛單的前10筆
      "doneOrders":[{ __與上方的掛單型態一樣__ }]
    }
  }
```

#### 4. sendOrderAsync  
傳輸`createOrderAsync`產生的掛單資料到伺服器。

輸入：
   
  - order: `Order` 由`createOrderAsync`產生的資料

輸出：
```json
  {
    "result": Boolean,
    "data": String
  }
```

#### 5. sendCancelOrderAsync  
傳輸`cancelOrderAsync`產生的掛單資料到伺服器。

輸入：
   
  - order: `MessageForCancel` 由`cancelOrderAsync`產生的資料

輸出：
```json
  {
    "result": Boolean,
    "data": String
  }
```

### 產生資料
#### 1. createOrderAsync  
創建掛單

輸入：
  
  - baseTokenAddress: `String` 基準幣的地址
  - baseTokenAmount: `String` 交易多少顆基準幣
  - baseTokenDecimal: `Number` 基準幣的精度
  - tradingTokenAddress: `String` 交易的代幣地址
  - tradingTokenAmount: `String` 交易的代幣數量
  - tradingTokenDecimal: `Number` 交易的代幣精度
  - isBuyOrder: `Boolean` true=買單， false=賣單
  - expirationInMin: `Number` 掛單在多少分鐘後過期

輸出： `Promise<Order>` 傳輸給伺服器的掛單資料，勿修改

#### 2. cancelOrderAsync  
取消掛單

輸入：
  
  1. orderHash: `String` 掛單的hash值

輸出： `Promise<MessageForCancel>` 傳輸給伺服器的取消掛單資料，勿修改
