const assert = require("assert");
const StarCreator = require("../index");

const TestConfig = {
  apiKey: 5566,//register with starbit
  privateKey: "3a1076bf45ab87712ad64ccb3b10217737f7faacbf2872e88fdd9a537d8fe266",
  rpcUrl: "https://mainnet.infura.io/v3/be609ccb25a14309a7d4e999b2ade343",
}


describe("Test Basic", function () {
  let starInstance;
  before(function (done) {
    this.timeout(100000);
    StarCreator(TestConfig.apiKey, TestConfig.privateKey, TestConfig.rpcUrl)
      .then(star => {
        starInstance = star;
      }).then(done)
      .catch(done);
  });

  this.timeout(5000);
  describe("Initial info", function () {
    it("should have defaultAccount property", function () {
      let wallet = "0xc2d7cf95645d33006175b78989035c7c9061d3f9";
      assert.equal(starInstance.defaultAccount, wallet);
    });
  })

  describe("Server api", function () {
    it("should receive token list, base on WETH", function (done) {
      let sbt = "0x503f9794d6a6bb0df8fbb19a2b3e2aeab35339ad",
        weth = "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2";
      starInstance.getAllTokenAsync()
        .then(response => {
          let wethResult = response.data.find(v => v.tokenAddress === weth);
          let sbtResult = response.data.find(v => v.tokenAddress === sbt);
          assert.equal(response.result, true);
          assert.equal(response.data.length > 0, true);
          assert.equal(!!wethResult, true);
          assert.equal(!!sbtResult, true);
          assert.equal(wethResult.toStable.WETH.volume, "--", "trading volume should be '--' when base on weth");
        }).then(done)
        .catch(done);
    });

    it("should receive token list, base on SBT", function (done) {
      let sbt = "0x503f9794d6a6bb0df8fbb19a2b3e2aeab35339ad",
        weth = "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2";
      starInstance.getAllTokenAsync(sbt)
        .then(response => {
          let wethResult = response.data.find(v => v.tokenAddress === weth);
          let sbtResult = response.data.find(v => v.tokenAddress === sbt);
          assert.equal(response.result, true);
          assert.equal(response.data.length > 0, true);
          assert.equal(!!wethResult, true);
          assert.equal(!!sbtResult, true);
          assert.equal(!!wethResult.toStable.WETH, true);
          assert.equal(!!wethResult.toStable.SBT, true);
        }).then(done)
        .catch(done);
    });

    it("should receive order list", function (done) {
      let sbt = "0x503f9794d6a6bb0df8fbb19a2b3e2aeab35339ad",
        weth = "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2";
      starInstance.getOrderBookAsync(weth, sbt)
        .then(response => {
          assert.equal(response.result, true);
          assert.equal(response.data.buy.length >= 0, true);
          assert.equal(response.data.sell.length >= 0, true);
        }).then(done)
        .catch(done);
    });

    it("should receive wallet's order or 'wallet address not found'", function (done) {
      starInstance.getWalletOrdersAsync()
        .then(response => {
          //maybe not any record about this wallet in server
          let otherSituation = response.result === false && response.data === "wallet address not found";
          assert.equal(response.result || otherSituation, true, `result should be true, data: "${response.data}"`);
        }).then(done)
        .catch(done);
    });
  });

  describe("Sign Order", function () {
    it("should create order", function (done) {
      let sbt = "0x503f9794d6a6bb0df8fbb19a2b3e2aeab35339ad",
        weth = "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2";
      this.timeout(5000);
      starInstance.createOrderAsync(
        weth,//baseToken
        0.182,//baseTokenAmount
        18,//baseTokenDecimal
        sbt,//tradingToken
        2000,//tradingTokenAmount
        18,//tradingTokenDecimal
        false,//isBuyOrder
        60//expirationInMin
      ).then(order => {
        assert.equal(order.makerAddress, starInstance.defaultAccount);
        assert.equal(order.makerAssetToken, sbt);
        assert.equal(order.takerAssetToken, weth);
        assert.equal(order.balance, "2000000000000000000000")
        assert.equal(order.takerAssetAmount, "182000000000000000")
      }).then(done)
        .catch(done);
    });

    it("should sign order for cancel", function (done) {
      let orderHash = "0x64e1cb8e81ebaf1a017fab918d9473b3caedea2031e8160e0be68b7d19017b96",
        signedOrderHash = "0xdae51465b1210055815f189f9059a31bda65f04628613f0097d3ae5ff61c552e67dc5e9f59c476d57d70bbc6a96ebb265835679d8f14d07e105cf8f9c97e762101";
      starInstance.cancelOrderAsync(orderHash)
        .then(result => {
          assert.equal(result.maker, starInstance.defaultAccount);
          assert.equal(result.orderHash, orderHash);
          assert.equal(result.signedMessage, signedOrderHash);
        }).then(done)
        .catch(done);
    });
  });
});
